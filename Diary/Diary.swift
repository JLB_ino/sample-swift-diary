//
//  Diary.swift
//  Diary
//
//  Created by LinuxAcademy on 2016/10/31.
//  Copyright © 2016年 LinuxAcademy. All rights reserved.
//

import Foundation
import RealmSwift

// DiaryのModelクラス
// Objectクラスを継承しているためKVOできる
class Diary: Object {
	
	// ----- プロパティ -----
	// 日記のID
	dynamic var id = 0
	
	// 日記のタイトル
	dynamic var title = ""
	
	// 日記の本文
	dynamic var body  = ""
	
	// 日記の投稿日
	dynamic var date  = ""
	
	
	// ----- 関数 -----
	// 関数①：PrimaryKey通知処理（RealmSwiftのprimaryKeyをoverride）
	override static func primaryKey() -> String? {
		
		// 1. 文字列”id”を返却
		return "id"
	}
}