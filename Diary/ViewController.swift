//
//  ViewController.swift
//  Diary
//
//  Created by LinuxAcademy on 2016/10/31.
//  Copyright © 2016年 LinuxAcademy. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

	// ------ プロパティ -----
	// 日記の一覧部分のオブジェクト
	@IBOutlet weak var tableView: UITableView!
	// Realmクラスのオブジェクト
	let realm = try! Realm()
	// 日記の一覧を降順で取得
	let dataArray = try! Realm().objects(Diary).sorted("date", ascending: false)
	
	
	// ----- 関数 -----
	override func viewDidLoad() {
		super.viewDidLoad()
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	
	
	// 関数①：一覧リロード処理（UIViewControllerのviewWillAppearをoverride）
	override func viewWillAppear(animated: Bool) {
		
		// 1. 親クラスのviewWillAppearを実行
		super.viewWillAppear(animated)
		
		// 2. 変数tableViewからリロード処理を実行
		tableView.reloadData()
	}
	
	
	// 関数②：画面表示処理（UIViewControllerのprepareForSegueをoverride）
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		
		// 1. 変数segueから取得したInputViewControllerオブジェクトを取得し、定数inputViewControllerに代入
		
		let inputViewController: InputViewController =
			segue.destinationViewController as! InputViewController

		// 2. 変数segueにセットされた宛先のIDを判定
		if segue.identifier == "cellSegue" {
			
			// 2-1. IDがcellSegueの場合（編集処理）
			// 2-1-1. 変数tableViewからタップされた記事の情報を取得し、定数indexPathに代入
			let indexPath = self.tableView.indexPathForSelectedRow
			
			// 2-1-2. 定数indexPathを使用し、タップされた記事を定数inputViewControllerに代入
			inputViewController.diary = dataArray[indexPath!.row]
			
		} else {
			
			// 2-2. IDがcellSegue以外の場合（登録処理）
			// 2-2-1. Diaryクラスのインスタンスを生成し、定数diaryに代入
			let diary = Diary()
			
			// 2-2-2. 日記一覧の件数を判定
			if dataArray.count != 0 {
				
				// 2-2-2-1. 日記一覧の件数が「0」ではない場合
				// 2-2-2-1-1. 定数diaryにIDを代入（設定値は日記一覧の最大値+1）
				diary.id = dataArray.max("id")! + 1
			}
			
			// 2-2-3. 定数diaryを定数inputViewController内の値として代入
			inputViewController.diary = diary
		}
	}
	
	
	// ----- TableViewのアクション -----
	// 関数③：日記一覧の行数カウント処理
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		
		// 1. 日記一覧の行数をカウントして返却
		return dataArray.count
	}
	
	
	// 関数④：行情報の作成処理
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		
		// 1. UITableViewCellのオブジェクトを作成し、定数cellに代入
		let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "Cell")
		
		// 2. 引数indexPathの行情報を添え字に日記一覧の情報を取得し、定数objectに代入
		let object = dataArray[indexPath.row]
		
		// 3. 定数cellにタイトルと日付の値を代入
		cell.textLabel?.text = object.title			// タイトル
		cell.detailTextLabel?.text = object.date	// 日付
		
		// 4. 定数cellを返却
		return cell
	}
	
	
	// 関数⑤：行情報の削除表示処理
	func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
		
		// 1. UITableViewCellEditingStyleから削除のステータスを返却
		return UITableViewCellEditingStyle.Delete
	}
	
	
	// 関数⑥：行削除処理
	func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
		
		// 1. 引数 editingStyleを判定
		if editingStyle == UITableViewCellEditingStyle.Delete {
			
			try! realm.write {

				// 1-1. Deleteの場合
				// 1-1-1. 変数realmのwrite関数を実行
				self.realm.delete(self.dataArray[indexPath.row])
				
				// 1-1-2. 引数tableViewのdeleteRowsAtIndexPaths関数を実行
				tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
			}
		}
	}
	
	
	// 関数⑦：行削除後処理
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		
		// 1. 指定したID（cellSegue）でperformSegueWithIdentifierを実行
		performSegueWithIdentifier("cellSegue", sender: nil)
	}
}