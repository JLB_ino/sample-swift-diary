//
//  InputViewController.swift
//  Diary
//
//  Created by LinuxAcademy on 2016/10/31.
//  Copyright © 2016年 LinuxAcademy. All rights reserved.
//

import UIKit
import RealmSwift

class InputViewController: UIViewController {

	// ----- プロパティ -----
	// タイトル入力フィールドのオブジェクト
	@IBOutlet weak var titleTextField: UITextField!
	// 本文入力フィールドのオブジェクト
	@IBOutlet weak var textView: UITextView!
	// Realmクラスのオブジェクト
	let realm = try! Realm()
	// Diaryクラスのオブジェクト
	var diary: Diary!
	
	
	// ----- 関数 -----
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
	
	
	// 関数①：初期表示処理（UIViewControllerのloadViewをoverride）
	override func loadView() {
		
		// 1. 親クラスのloadViewを実行
		super.loadView()
		
		// 2. 画面に表示する値を変数diaryに保持しているタイトルと本文から取得して代入
		self.titleTextField.text = self.diary.title
		self.textView.text = self.diary.body
	}
	
	
	// 関数②：保存処理
	@IBAction func save(sender: UIButton) {
		
		try! realm.write {
			
			// 1. 画面からタイトルと本文を取得して変数diaryにセット
			self.diary.title = self.titleTextField.text!
			self.diary.body  = self.textView.text
			
			// 2. 保存日時を日本時間で取得するためNSDateFormatterオブジェクトを作成
			let formatter = NSDateFormatter()
			formatter.locale = NSLocale(localeIdentifier: "ja_JP")
			formatter.dateStyle = NSDateFormatterStyle.ShortStyle
			formatter.timeStyle = NSDateFormatterStyle.ShortStyle
			
			// 3. 変数diaryのdateにフォーマットした現在時刻を代入
			self.diary.date  = formatter.stringFromDate(NSDate())
			
			// 4. 変数realmに保存
			self.realm.add(self.diary, update: true)
		}
		
		// 5. NavigationControllerを表示
		self.navigationController?.popViewControllerAnimated(true)
	}
}